import React from 'react';
import { FaTimes, FaWindowMaximize, FaWindowMinimize } from 'react-icons/fa';
import {remote} from 'electron';
import Button from './Button';

const ButtonBar = ({side}) => {
  const window = remote.getCurrentWindow();
  const maximize = window.isMaximized
    ? window.unmaximize()
    : window.maximize();
  const style = styles[side];

  return (
    <div style={style}>
      <Button
        onClick={window.close}
        background='rgba(225,25,25,0.9)'
      >
        <FaTimes />
      </Button>
      <Button onClick={window.minimize}>
        <FaWindowMinimize />
      </Button>
      <Button onClick={maximize}>
        <FaWindowMaximize />
      </Button>
    </div>
  );
};

const styles = {
  'right': {
    display: 'flex',
    flexDirection: 'row',
    order: 1,
    positon: 'absolute',
    right: '5px'
  },
  'left': {
    display: 'flex',
    flexDirection: 'row-reverse',
    order: 0,
    positon: 'absolute',
    left: '5px'
  }
}

export default ButtonBar;