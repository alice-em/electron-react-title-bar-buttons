import React from 'react';
import PropTypes from 'prop-types';
import process from 'process';

import ButtonBar from './ButtonBar';
import Title from './Title';

const Titlebar = ({
  buttonStyles,
  title,
  titleStyles,
}) => {
  const OS = process.platform;
  const style = styles[OS];
  const side = sides[OS];
  const mergedStyles = {
    ButtonBar: {...style.ButtonBar, ...buttonStyles},
    Title: {...style.Title, ...titleStyles}
  }

  return (
    <div style={{display: 'flex'}}>
      <ButtonBar
        style={style.ButtonBar}
        side={side.ButtonBar}
      />
      <Title
        style={mergedStyles.ButtonBar}
        title={title}
      />
    </div>
  );
}

Titlebar.propTypes = {
  title: PropTypes.string.isRequired
}

const sides = {
  'darwin': {
    'ButtonBar': 'left',
    'Title': 'right'
  },
  'linux': {
    'ButtonBar': 'left',
    'Title': 'right'
  },
  'win32': {
    'ButtonBar': 'right',
    'Title': 'left'
  },
}

const styles = {
  'darwin': {},
  'linux': {},
  'win32': {},
}

export default Titlebar;
