import React from 'react';
import PropTypes from 'prop-types';

const Title = ({side, title}) => {
  const style = styles[side];
  return (
    <div style={style}>
      {title}
    </div>
  )
}

const styles = {
  'right': {
    display: 'flex',
    flexDirection: 'row',
    order: 1,
  },
  'left': {
    display: 'flex',
    flexDirection: 'row-reverse',
    order: 0,
  }
}

Title.propTypes = {
  side: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
}

export default Title;