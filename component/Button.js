import React from 'react';
import PropTypes from 'prop-types';
import process from 'process';

const Button = ({backgroundColor, children, onClick}) => {
  const OS = process.platform;
  const style = styles[OS];

  if (backgroundColor !== null) {
    style.backgroundColor = backgroundColor;
  }

  return (
    <button
      onClick={onClick}
      style={style}
      type="button"
    >
      {children}
    </button>
  )
};

Button.defaultProps = {
  backgroundColor: null,
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  children: PropTypes.element.isRequired,
  onClick: PropTypes.func.isRequired,
};

const styles = {
  'darwin': {
    backgroundColor: 'rgba(1,1,1,0.2)',
    margin: '2px',
    borderRadius: '50%',
    height: '10px',
    width: '10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  'linux': {
    backgroundColor: 'rgba(1,1,1,0.2)',
    margin: '2px',
    borderRadius: '50%',
    height: '10px',
    width: '10px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  'win32': {
    backgroundColor: 'rgba(1,1,1,0.2)',
    height: '10px',
    width: '13px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
};